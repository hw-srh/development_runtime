#!/usr/bin/env bash

set -o pipefail # trace errors through pipes
set -o errtrace # trigger error in error trap even if the error occurs in a subshell
set -o nounset  # exit the script if an uninitialized variable is used
set -o errexit  # exit the script if a statement returns an exit code other than 0

function check_dependencies() {

  declare -A required_dependencies=(
    [docker]='Missing docker'
    ['docker-compose']='Missing docker-compose'
    [node]='Missing nodejs (use version 11 or later!)'
    [npm]='Missing npm'
    [git]='Missing git'
    [awk]='Missing awk'
  )

  declare -A useful_dependencies=(
    [nest]='Missing optional program, nestjs cli, install it with sudo npm i -g @nestjs/cli"'
    [ncu]='Missing optional program npm-check-updates package, install it with sudo npm i -g npm-check-updates'
    [ssh]='Missing optional program ssh'
  )

  for dependency_name in "${!required_dependencies[@]}"; do
    if ! command -v "${dependency_name}" &>/dev/null; then
      echo -e "ERROR\n"
      echo "${required_dependencies[${dependency_name}]}"
      exit 1
    fi
  done

  for dependency_name in "${!useful_dependencies[@]}"; do
    if ! command -v "${dependency_name}" &>/dev/null; then
      echo -e "WARNING\n"
      echo "${useful_dependencies[${dependency_name}]}"
    fi
  done
}

function get_path_to_script() {
  realpath "$(dirname "$0")"
}

function get_current_user_id() {
  id -u
}

function get_shared_docker_compose_command_prefix() {
  local script_dir="$(get_path_to_script)"

  # multiple --env-file declarations are currently not supported  (version 1.27.4)  --env-file ${script_dir}/.development.env \
  # export $(xargs < .env) is a workaround
  echo "export \$(xargs < ${script_dir}/.development.env) && \
        docker-compose \
        --file ./docker_production_runtime/docker-compose.yml \
        --file docker-compose.dev.yml \
        --project-directory ${script_dir} \
        --env-file ${script_dir}/docker_production_runtime/.env \
        --compatibility"
}

function setup_dev_env() {
  eval $(ssh-agent) 2>/dev/null
  ssh-add 2>/dev/null
  git submodule update --init --recursive && git submodule foreach --recursive 'git checkout master && git pull'
  (cd project && npm install && npm run build)
  (cd project/client && npm install && npm run build)
  build_docker_setup
}

function start_docker_setup() {
  export HOST_UID="$(get_current_user_id)" &&
    eval "$(get_shared_docker_compose_command_prefix) up --detach"
}

function stop_docker_setup() {
  eval "$(get_shared_docker_compose_command_prefix) down"
}

function build_docker_setup() {
  eval "$(get_shared_docker_compose_command_prefix) build --pull --parallel"
}

function watch_node_logs() {
  echo "Watching nodejs server logs...(only new logs from now on)"
  docker logs --follow --since "$(date +"%s")" "$(docker ps --filter="name=_node_1" | tail -n 1 | awk '{print $13}')"
}

function handle_flags() {

  case "${1:-}" in
  --setup)
    setup_dev_env
    ;;
  --up)
    start_docker_setup
    ;;
  --down)
    stop_docker_setup
    ;;
  --build)
    build_docker_setup
    ;;
  --watch)
    watch_node_logs
    ;;
  *)
    false
    ;;
  esac
}

function print_help_message() {
  echo "./run.sh --setup                               run git pull in submodules and install php/js dependencies"
  echo "./run.sh --up                                  runs docker-compose up"
  echo "./run.sh --down                                stops the docker setup"
  echo "./run.sh --build                               rebuilds the docker images defined in docker-compose"
  echo "./run.sh --watch                               watch the nodejs docker container logs"
}

function check_if_docker_is_running() {
  if ! docker info &>/dev/null; then
    echo "Docker is not running!"
    exit 1
  fi
}

function main() {
  check_dependencies
  check_if_docker_is_running

  handle_flags "${@}" && exit 0

  print_help_message
}

main "${@}"
