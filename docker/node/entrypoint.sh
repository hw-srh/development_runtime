#!/bin/sh

set -o nounset ## exit the script if an uninitialized variable is used
set -o errexit ## exit the script if a statement returns an exit code other than 0

change_project_uid_to_host_uid() {
  docker_user_id="$(id -u node)"
  HOST_UID="${HOST_UID:-}"

  if [ "${docker_user_id}" != "${HOST_UID}" ]; then
    usermod -u "${HOST_UID}" node
    groupmod -g "${HOST_UID}" node

    find / -path /sys -prune -o -path /proc -prune -o -group "${docker_user_id}" -exec chgrp -h node {} \;
    find / -path /sys -prune -o -path /proc -prune -o -user "${docker_user_id}" -exec chown -h node {} \;
  fi

  chown node:node -R /var/www/project
}

reinstall_node_modules_with_musl_libc_support() {
  if ! ldd node_modules/bcrypt/lib/binding/napi-v3/bcrypt_lib.node 2>/dev/null | tail -n1 | awk '{print $1}' | grep musl 1>/dev/null; then
    echo "WARNING reinstalling node_modules with musl libc support!"
    echo "this project might not work outside docker unless you"
    echo "reinstall node_modules from the host os"

    rm -rf node_modules
    npm install
  fi
}

main() {
  # for some reason the node users path is empty after installing shadow, which we need for the dev setup to adjust permissions
  path="${PATH}:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

  cd /var/www/project

  if [ "${APP_ENV:-}" = 'dev' ] || [ "${APP_ENV:-}" = 'development' ]; then
    change_project_uid_to_host_uid
    reinstall_node_modules_with_musl_libc_support
    exec su node -c "export PATH=\"${path}\" && exec npm run start:debug 2>&1"
  else
    # a proper production container should start with the node user
    # and then run node dist/main directly to forward the sigterm signal without using exec
    exec su node -c "export PATH=\"${path}\" && exec npm run start:prod 2>&1"
  fi
}

main "${@}"
