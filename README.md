# About this Project

## System requirements

### Required

* Linux / Windows (through wsl2)
* Docker and docker-compose (>=1.27)
* Bash (only for the run script)
* nodejs
* npm
* git

### Optional

PHPMYADMIN http://localhost:8080/
  - test@test.de / change_it_asap!
CLIENT START http://localhost:3000/app/home
CLIENT PROD https://localhost

### Optional

* [nestjs cli](https://docs.nestjs.com/cli/overview)
* ncu ([npm-check-updates](https://www.npmjs.com/package/npm-check-updates))
* openssh (required for ssh-agent and ssh-add, so that the git credentials do not have to be entered multiple times
  during setup)

## How to use this project

### System setup / installation (development only)

* `./run.sh --setup`

### First steps

1. Edit `.env` in shared_docker_base repo or `.development.env` in this repository to modify SMTP credentials, admin
   user credentials, server ports and more


2. Use the run script to start the docker development setup with: ` ./run.sh --up`


3. The first startup can take a while, you might have to wait up to 5 minutes


3. Access the servers in your browser on whatever port is configured in `.env` or `.development.env`

### nodejs debugging

* webstorm (jetbrains IDEs)
  See this [issue](https://github.com/nestjs/nest/issues/993#issuecomment-461189430)

* vscode

```
{
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
      {
        "type": "node",
        "request": "attach",
        "name": "Attach NestJS WS",
        "port": 9229,
        "restart": true,
        "stopOnEntry": false,
        "protocol": "inspector"
      }
    ]
  }
```

### Additional development information

#### Problems with bcrypt and Nodejs alpine docker image

    Alpine linux uses musl libc, which means that all node packages with gnu libc bindings have to be build
    against musl or the available prebuild binary has to be downloaded (will be done automatically when running npm install)
    in case the binaries have been build against gnu libc the container will reinstall the node_modules during container start.

    If you want to run the setup outside of docker you have to do one of the following:

    * install musl libc on your system
    * reinstall the node_modules without restarting the docker container (this will then use your systems default c library)
    * remove the nodejs container from docker-compose

## Software Stack

### Middleware

* MariaDB
* Haproxy (load balance for the nodejs servers)
* Redis (tracking JWT ids)

### Core project

#### Backend

* nestjs (nodejs + TypeScript)
* passport (authentication)
* bcrypt.js (password hashing)
* typeorm

#### Frontend
* ReactJs + TypeScript
* ReactRouter
* axios (promise based http client)